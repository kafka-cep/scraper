package scrape

import (
	"context"
	"fmt"
	"go.uber.org/zap"
	"time"
)

type Service struct {
	*zap.Logger

	scrapeInterval time.Duration
	scrapers       []Scraper
}

func NewServiceScrapers(logger *zap.Logger, conf *Config, creator ScrapersCreator) (*Service, error) {
	var err error

	res := &Service{
		Logger: logger,
	}

	res.scrapers, err = creator(logger, conf)
	if err != nil {
		return nil, err
	}

	res.scrapeInterval, _ = time.ParseDuration(conf.ScrapeInterval)

	return res, nil
}

func (sc *Service) Run(ctx context.Context, send chan<- []byte, workCnt int) {
	scrapeCh := make(chan Scraper)

	go func() {
		defer close(scrapeCh)

		for {
			select {
			case <-ctx.Done():
				return
			default:
				for _, scrape := range sc.scrapers {
					scrapeCh <- scrape
				}
			}
			time.Sleep(sc.scrapeInterval)
		}
	}()

	for i := 0; i < workCnt; i++ {
		go func() {
			for val := range scrapeCh {
				select {
				case <-ctx.Done():
					return
				default:
					res, err := val.Scrape()
					if err != nil {
						sc.Error(fmt.Sprintf("Cannot scrape data: %v", err))
					}
					send <- res
				}
			}
		}()
	}
}

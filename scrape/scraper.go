package scrape

import (
	"go.uber.org/zap"
)

type Scraper interface {
	Scrape() ([]byte, error)
}

type ScrapersCreator func(logger *zap.Logger, conf *Config) ([]Scraper, error)

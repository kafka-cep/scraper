package logger

import "os"

type Config struct {
	Level  string `yaml:"log_level" validate:"required,oneof=debug info warn error"`
	Format string `yaml:"log_format" validate:"required,oneof=json console"`
}

func (lc *Config) SetDefaults() {
	lc.Level = "info"
	lc.Format = "json"
}

func (lc *Config) ENVS() {
	if val, ok := os.LookupEnv("LOG_LEVEL"); ok {
		lc.Level = val
	}

	if val, ok := os.LookupEnv("LOG_FORMAT"); ok {
		lc.Format = val
	}
}

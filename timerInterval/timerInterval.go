package timerInterval

import (
	"context"
	"time"
)

type TimerInterval struct {
	startTime time.Time
	in        chan struct{}
	out       chan []float64
	done      bool
	startLong float64
	startLat  float64
	stepLong  float64
	stepLat   float64
}

func New(start time.Time) *TimerInterval {
	return &TimerInterval{
		startTime: start,
		in:        make(chan struct{}),
		done:      true,
	}
}

func (tm *TimerInterval) Init(startLong, startLat, endLong, endLat float64, duration time.Duration) {
	if !tm.done {
		return
	}

	tm.done = false
	tm.startTime = time.Now()
	tm.in = make(chan struct{})
	tm.out = make(chan []float64)
	tm.startLong = startLong
	tm.startLat = startLat
	tm.stepLong = (endLong - startLong) / float64(duration.Milliseconds())
	tm.stepLat = (endLat - startLat) / float64(duration.Milliseconds())

	ctx, _ := context.WithTimeout(context.Background(), duration+time.Millisecond*10)
	go tm.timerStart(ctx)

}

func (tm *TimerInterval) timerStart(ctx context.Context) {
	defer func() {
		tm.done = true
		close(tm.out)
	}()

	for {
		select {
		case <-ctx.Done():
			return
		case <-tm.in:
			long, lat := tm.calculateCurPos()
			tm.out <- []float64{long, lat}
		default:
			continue
		}
	}
}

func (tm *TimerInterval) calculateCurPos() (float64, float64) {
	curDur := time.Since(tm.startTime)

	curLong := tm.stepLong * float64(curDur.Milliseconds())
	curLat := tm.stepLat * float64(curDur.Milliseconds())
	return tm.startLong + curLong, tm.startLat + curLat
}

func (tm *TimerInterval) GetCoords() (float64, float64) {
	if tm.done {
		close(tm.in)
		return 0, 0
	}
	tm.in <- struct{}{}
	results := <-tm.out
	return results[0], results[1]
}

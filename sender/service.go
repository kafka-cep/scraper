package sender

import (
	"context"
	"go.uber.org/zap"
)

type Service struct {
	*zap.Logger

	sender Sender
}

// TODO: Возможно добавлять сюда имя типа для фабрики излишне
func NewSenderService(logger *zap.Logger, conf *Config, senderType string) (*Service, error) {
	var err error

	serv := &Service{
		logger,
		nil,
	}

	serv.sender, err = SenderFactory(senderType, logger, conf)

	return serv, err
}

func (serv *Service) Run(ctx context.Context, workerCnt int, out <-chan []byte) {
	for i := 0; i < workerCnt; i++ {

		go func() {
			for val := range out {
				select {
				case <-ctx.Done():
					return
				default:
					serv.sender.Send(val)
				}
			}
		}()
	}
}

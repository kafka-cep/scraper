package sender

import (
	"go.uber.org/zap"
)

type Sender interface {
	Send([]byte)
}

func SenderFactory(name string, logger *zap.Logger, config *Config) (Sender, error) {
	switch name {
	case "mock":
		return NewMockSender(logger, config)
	default:
		return NewURLSender(logger, config)
	}
}

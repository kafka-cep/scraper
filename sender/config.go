package sender

import "os"

type Config struct {
	URL string `yaml:"sender_url" validate:"required,url"`
}

func (sc *Config) ENVS() {
	if val, ok := os.LookupEnv("SENDER_URL"); ok {
		sc.URL = val
	}
}

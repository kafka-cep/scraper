package sender

import (
	"fmt"
	"go.uber.org/zap"
)

type MockSender struct {
	logger *zap.Logger
}

func NewMockSender(logger *zap.Logger, conf *Config) (*MockSender, error) {
	res := &MockSender{
		logger: logger,
	}

	return res, nil
}

func (ag *MockSender) Send(data []byte) {
	fmt.Println(string(data))
}
